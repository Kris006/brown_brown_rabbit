import "./App.css";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Sponsors from "./components/Sponsors/Sponsors";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div className="App">
      <div className="MainWebsite">
        <Header />
        <Main />
        <Sponsors />
        <Footer />
      </div>
    </div>
  );
}

export default App;
