import React from "react";
import classes from "./SocialMedia.module.css";
import SocialMediaLogos from "./SocialMediaLogos/SocialMediaLogos";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import YouTubeIcon from "@material-ui/icons/YouTube";
import InstagramIcon from "@material-ui/icons/Instagram";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import { withStyles } from "@material-ui/core/styles";

const SocialMedia = () => {
  const socialMedia = [
    { twitter: <TwitterIcon style={{ fontSize: "2rem", color: "white" }} /> },
    { facebook: <FacebookIcon style={{ fontSize: "2rem", color: "white" }} /> },
    { youtube: <YouTubeIcon style={{ fontSize: "2rem", color: "white" }} /> },
    {
      instagram: <InstagramIcon style={{ fontSize: "2rem", color: "white" }} />,
    },
    {
      email: (
        <AlternateEmailIcon style={{ fontSize: "2rem", color: "white" }} />
      ),
    },
  ];

  return (
    <div className={classes.SocialMedia}>
      {socialMedia.map((svg, index) => {
        return <SocialMediaLogos image={svg} key={index} />;
      })}
    </div>
  );
};

export default SocialMedia;
