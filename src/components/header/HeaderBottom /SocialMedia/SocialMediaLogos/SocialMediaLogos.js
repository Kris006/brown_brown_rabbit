import React from "react";
import classes from "./SocialMediaLogos.module.css";

const Logos = (props) => {
  const result = Object.keys(props.image).map((image) => {
    return <div className={classes.Logo}>{props.image[image]}</div>;
  });

  return <div className={classes.Logos}>{result}</div>;
};
export default Logos;
