import React from "react";
import classes from "./Heading.module.css";

const Heading = () => {
  return (
    <div className={classes.Heading}>
      <img src="../assets/HeaderLogo/Logo.png" alt="logo" />
    </div>
  );
};

export default Heading;
