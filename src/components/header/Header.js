import React, { Component } from "react";
import classes from "./Header.module.css";
import Heading from "./Heading/Heading";
import NavBar from "./NavBar/NavBar";
import SearchBar from "./SearchBar/SearchBar";
import Images from "./ImagesRandom/ImagesRandom";
import Quote from "./HeaderBottom /HeaderQuotes/HeaderQuote";
import SocialMedia from "./HeaderBottom /SocialMedia/SocialMedia";
import ModalMenu from "./Modal/Modal";

class Header extends Component {
  state = {
    nav: [
      "About NBC",
      "2011 Event",
      "Nordic Roaster",
      "Result",
      "Links",
      "Contact",
    ],
  };
  // Search Bar and Nav bar might need to be adjusted to move the classes to their own components
  render() {
    return (
      <div className={classes.Header}>
        <Heading />
        <div className={classes.NavBar_SearchBar}>
          <SearchBar />
          <NavBar nav={this.state.nav} />
          <ModalMenu />
        </div>
        <Images />
        <Quote />
        <SocialMedia />
      </div>
    );
  }
}

export default Header;
