import React from "react";
import classes from "./ImagesRandom.module.css";

const Image = () => {
  const test = Math.ceil(Math.random() * 4);

  return (
    <div className={classes.Image}>
      <img src={`../assets/headerImages/picture-${test}.jpg`} alt="coffee" />
    </div>
  );
};

export default Image;
