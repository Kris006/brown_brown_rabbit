import React from "react";
import classes from "./Sponsors.module.css";
import SponsorImages from "./SponsorImages/SponsorImages";
import Images from "../Data/SponsorImageData/SponsorImagData";

const Sponsors = () => {
  return (
    <div className={classes.Sponsors}>
      <h2>Nordic Barista Cup Sponsors</h2>
      {Images.map((img, index) => {
        console.log(img);
        return <SponsorImages image={img} key={index} />;
      })}
    </div>
  );
};

export default Sponsors;
