import React from "react";
import classes from "./Footer.module.css";
import FooterImagesData from "../Data/FooterData/FooterData";
import FooterImages from "./FooterImages/FooterImages";
import SocialMedia from "../Header/HeaderBottom /SocialMedia/SocialMedia";

const Footer = () => {
  return (
    <div className={classes.Footer}>
      <div className={classes.AboutFooter}>
        <h3>About Nordic Bartita Cup</h3>
        <p>The vision within the Nordic Barsita Cup is :</p>
        <p className={classes.paragraph}>
          "To create an environment in which knowledge about coffee and it's
          sphere can be obtained"
        </p>
        <p>
          "..create an environment.."
          <br />
          Combined with persinally absorption having the opportunity to see and
          experience countries, people, traditions etc. will always serve as a
          source of inspiration in our daily work. The organization behind the
          Nordic Barista Cup see it as its main purpose to be part of creating
          this forum in which peoiple can meet, biind and achieve further
          knowledge.
        </p>
      </div>
      <div className={classes.ImagesFooter}>
        <h3>
          <span>Nbc</span> Flickr Stream
        </h3>
        {FooterImagesData.map((image, index) => {
          return <FooterImages image={image} key={index} />;
        })}
      </div>
      <div className={classes.ContactFooter}>
        <h3>Contact</h3>
        <p className={classes.paragraph}>Nordic Barista Cup</p>
        <address>
          Amagertorv 13 <br />
          1160 Copenhagen K<br />
          +45 33 23 04 28
          <br />
          CVR: 11425677
          <br />
          Email: bbrend@nordicbaristacup.dk
        </address>

        <div className={classes.SocialMedia}>
          {" "}
          <SocialMedia />
        </div>
      </div>
    </div>
  );
};

export default Footer;
