import React from "react";
import classes from "./SideBar.module.css";

const SideBar = () => {
  const countryList = [
    "2011 - ?",
    "2010 - Sweden",
    "2009 - Denmark",
    "2007 - Sweden",
    "2006 - Norway",
    "2005 - Norway",
    "2004- Denmark ",
  ];

  return (
    <div className={classes.SideBar}>
      <div>
        <h3>
          <span>Nbc</span> Shop
        </h3>
        <p>Your shopping cart is empty</p>
        <a href="/">Visit the shop</a>
      </div>
      <div>
        <h3>Next Event</h3>
        <address>
          <span>Nordic Barista Cup</span> 2011
          <br />
          Copenhagen, Denmark <br />
          Dates : 25th - 27th August 2011
          <br /> Theme :<span>Sensory</span>
        </address>
        <a href="/">Sign up now</a>
      </div>
      <div>
        <h3>Scoreboard</h3>
        <p className={classes.scoreboard}>List of winners from past year</p>
        {countryList.map((countries, index) => {
          return (
            <ul>
              <li key={index}>{countries}</li>
            </ul>
          );
        })}
      </div>
    </div>
  );
};

export default SideBar;
