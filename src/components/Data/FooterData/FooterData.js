import React from "react";

const FooterImages = [
  { name: "snow", img: "../assets/FooterImages/pic1.png" },
  { name: "tree", img: "../assets/FooterImages/pic2.png" },
  { name: "flower", img: "../assets/FooterImages/pic3.png" },
  { name: "flower", img: "../assets/FooterImages/pic4.png" },
  { name: "snow", img: "../assets/FooterImages/pic5.png" },
  { name: "snow", img: "../assets/FooterImages/pic6.png" },
  { name: "snow-flower", img: "../assets/FooterImages/pic7.png" },
  { name: "tree", img: "../assets/FooterImages/pic8.png" },
  { name: "snow", img: "../assets/FooterImages/pic9.png" },
];

export default FooterImages;
